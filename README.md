Example [Gatsby](https://www.gatsbyjs.com/) website using GitLab Pages.

Learn more about GitLab Pages at https://docs.gitlab.com/ee/user/project/pages/.

---

This Gatsby example is based on <https://github.com/gatsbyjs/gatsby-starter-default>.

## GitLab CI/CD

This project's static Pages are built by [GitLab CI/CD](https://about.gitlab.com/gitlab-ci/), following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yml
stages:
  - build
  - deploy

# These folders are cached between builds
# http://docs.gitlab.com/ee/ci/yaml/#cache
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    # Default cache directory from https://classic.yarnpkg.com/en/docs/install-ci/#gitlab.
    - node_modules/
    # Both .cache and public must be cached, otherwise builds will fail
    - .cache/
    - public/

build:
  image: "node:20-bookworm-slim"
  stage: build
  script:
    # Specifically set the arch to download pre-compiled binaries of sharp
    # https://sharp.pixelplumbing.com/install#npm-v10
    - npm install --cpu=x64 --os=linux --libc=glibc sharp
    - npm install
    - npm run build
    - ls -la
  artifacts:
    paths:
      - public
    expire_in: 1 day
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_ID

pages:
  image: busybox
  stage: deploy
  needs:
    - build
  script:
    - echo "Deploying to GitLab Pages..."
  artifacts:
    paths:
      - public
    expire_in: 1 day
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

## Add path prefix to Gatsby

As the site will be hosted under `yourname.gitlab.io/examplerepository/`, you
will need to configure Gatsby to use the Path Prefix plugin.

In the `gatsby-config.js`, set the `pathPrefix` to be added to your site's link
paths. The `pathPrefix` should be the project name in your repository. For
example, if your project is `https://gitlab.com/yourname/examplerepository/`,
then your `pathPrefix` should be `/examplerepository`:

```js:title=gatsby-config.js
module.exports = {
  pathPrefix: `/examplerepository`,
}
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
1. [Install](https://www.gatsbyjs.com/docs/tutorial/getting-started/part-0/) Gatsby dependencies.
1. Generate and preview the website with hot-reloading: `gatsby develop`.
1. Add content.

Read more at Gatsby's [documentation](https://www.gatsbyjs.com/docs/).

## Did you fork this project?

If you forked this project for your own use, go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.
